#!/usr/bin/env python
# Copyright 2019 Major Hayden <major@mhtx.net>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This runs icanhazip.com and friends."""
import json
import socket
import subprocess
import time

import falcon


def check_address(remote_addr):
    """Check a remote address to verify it is legitimate."""
    try:
        socket.inet_pton(socket.AF_INET, remote_addr)
        return True
    except socket.error:
        pass

    try:
        socket.inet_pton(socket.AF_INET6, remote_addr)
        return True
    except socket.error:
        pass

    return False


def get_epoch(req, resp):
    """Return the epoch time."""
    return int(time.time())


def get_ip_address(req, resp):
    """Return the remote IP address."""
    return req.access_route[0]


def get_headers(req, resp):
    """Return a list of request headers."""
    return json.dumps(req.headers)


def get_proxy_headers(req, resp):
    """Return a list of proxy headers."""
    proxy_headers = [
        'via',
        'forwarded',
        'client-ip',
        'useragent_via',
        'proxy_connection',
        'xproxy_connection',
        'http_pc_remote_addr',
        'http_client_ip',
        'http_x_appengine_country'
        ]
    headers = {
        key: value for key, value in req.headers.items()
        if key.lower() in proxy_headers
    }
    return json.dumps(headers)


def get_ptr(req, resp):
    """Return the PTR record of the remote address."""
    remote_addr = req.access_route[0]
    try:
        result = socket.gethostbyaddr(remote_addr)
        return result[0]
    except Exception:
        return remote_addr


def get_traceroute(remote_addr, mtr_opts=None):
    """Return traceroute output."""
    if not check_address(remote_addr):
        return remote_addr

    traceroute_cmd = [
        'traceroute',
        '-q', '1',
        '-f', '2',
        '-w', '1'
    ]

    if mtr_opts:
        traceroute_cmd.append(mtr_opts)

    traceroute_cmd.append(remote_addr)
    return subprocess.check_output(traceroute_cmd).decode('utf-8')


def get_traceroute_no_dns(req, resp):
    """Return the traceroute without DNS."""
    mtr_opts = '-n'
    return get_traceroute(req.access_route[0], mtr_opts)


def get_traceroute_with_dns(req, resp):
    """Return the traceroute without DNS."""
    return get_traceroute(req.access_route[0])




ICANHAZ_HOSTS = {
    'icanhazepoch.com': get_epoch,
    'icanhazip.com': get_ip_address,
    'icanhazptr.com': get_ptr,
    'icanhazheaders.com': get_headers,
    'icanhazproxy.com': get_proxy_headers,
    'icanhaztrace.com': get_traceroute_no_dns,
    'icanhaztraceroute.com': get_traceroute_with_dns,
}


class ICanHaz(object):
    """The falcon class that handles requests."""

    def on_get(self, req, resp):
        """Handle GET requests."""
        resp.status = falcon.HTTP_200  # This is the default status

        # Which host header was passed?
        if req.host:
            try:
                host = next(
                    x for x in ICANHAZ_HOSTS.keys()
                    if req.host.endswith(x)
                )
            except StopIteration:
                host = 'icanhazip.com'
        else:
            host = 'icanhazip.com'

        # Respond with data.
        resp.content_type = falcon.MEDIA_TEXT
        resp.body = f"{ICANHAZ_HOSTS[host](req, resp)}\n"


# falcon.API instances are callable WSGI apps
app = falcon.API()

# Resources are represented by long-lived class instances
icanhaz = ICanHaz()

# things will handle all requests to the '/things' URL path
app.add_route('/', icanhaz)
