#!/bin/bash
set -euxo pipefail
cd /opt/icanhazip/
gunicorn -b 0.0.0.0:8000 -k sync -w 16 icanhazip:app &
/usr/sbin/nginx -t
/usr/sbin/nginx
