#!/bin/bash
set -euxo pipefail

echo "fastestmirror=true" >> /etc/dnf/dnf.conf
dnf -y upgrade
dnf -y install nginx python3 traceroute
dnf -y install gcc python3-devel redhat-rpm-config
curl -s -o /tmp/get-pip.py https://bootstrap.pypa.io/get-pip.py
python3 /tmp/get-pip.py
pip3 install cython
pip3 install --no-binary :all: falcon gunicorn
dnf -y history rollback 3
dnf clean all
